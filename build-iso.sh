#!/bin/bash -x

export TERM=xterm

ci_project_dir="$1"

[ -z "$ci_project_dir" ] && exit 1

# prepare arcolinux environment
git clone https://github.com/arcolinux/arcolinux-spices.git $ci_project_dir/arco-spices
cd $ci_project_dir/arco-spices/usr/share/arcolinux-spices/scripts/ && \
	./trust-key.sh && \
	./add-keyservers-for-key-importing.sh && \
	./add-arcolinux-repo-to-pacman-conf.sh && \
	pacman -Sy

# get my personal packaged arcolinuxb repo
git clone https://github.com/arcolinuxb/arco-i3 $ci_project_dir/arco-i3

# build the iso image
cd $ci_project_dir/arco-i3/installation-scripts && ./30-build-the-iso-the-first-time.sh
[ $? -ne 0 ] && exit 1

# copy iso image to ci project dir to make it available as an artifact
# (see .gitlab-ci.yml)
cp ~/ArcoLinuxB-Out/arcolinuxb*.iso /tmp
